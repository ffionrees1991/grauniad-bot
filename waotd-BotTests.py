from distutils.command.clean import clean
from mimetypes import init
from bs4 import BeautifulSoup
from datetime import date
import re
import requests.auth
import praw

content = requests.get('https://en.wikipedia.org/wiki/Wikipedia:Today%27s_featured_article')
soup = BeautifulSoup(content.text, 'html.parser')
divstring = soup.find('div', attrs={'class':"MainPageBG"})

hreflinks = divstring.findAll('a', href=True)

hrefLink = str(hreflinks[1])

pattern = '"(.*?)"'
links = ' '.join((re.findall(pattern,hrefLink)))
print(links)

# get title & date. pattern is everything after first whitespace
pattern = '(?<=\s).*'
title = ' '.join((re.findall(pattern,links)))
print(title)

# get & build url. patters is everything after '/wiki/' up until whitespace
pattern = '(\/wiki\S*)'
urlString = ' '.join((re.findall(pattern,links)))
print(urlString)

url = 'https://en.wikipedia.org/' + urlString
print(url)